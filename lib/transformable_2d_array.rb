class Transformable2DArray
  def initialize(size, default_element=nil)
    @inner_array = []
    @size = size
    (0...@size).each do |row|
      @inner_array[row] = []
      (0...@size).each do |col|
        @inner_array[row][col] = default_element
      end
    end
  end

  attr_reader :size

  def [](row, col)
    check_coordinates(row, col)
    @inner_array[row][col]
  end

  def []=(row, col, value)
    check_coordinates(row, col)
    @inner_array[row][col] = value
  end

  def line(line_number)
    @inner_array[line_number]
  end

  def set_values(array)
    check_size(array)
    @inner_array = array
  end

  def flip_horizontally!
    @inner_array.each{|a| a.reverse! }
  end

  def flip_vertically!
    @inner_array.reverse!
  end

  def transpose!
    coord_array = (0...@size).to_a
    coord_array.product(coord_array).each{|row, col|
      next if row < col
      self[row, col], self[col, row] = self[col, row], self[row, col]
    }
  end

  def to_a
    @inner_array
  end

  private 

  def check_coordinates(*coords)
    raise 'Coordinates must be >= 0!' if coords.any?{|c| c < 0}
    raise 'Coordinates cannot exceed size!' if coords.any?{|c| c >= @size}
  end

  def check_size(array)
    raise ArgumentError, 'Invalid number of rows!' if array.size != @size

    array.each{|line| 
      raise ArgumentError, 'Invalid number of values!' if line.size != @size
    }

  rescue ArgumentError => e
    raise "Array must have the exact size #{@size}x#{@size}!", e

  end

end