# encoding: UTF-8
class Cell

  InvalidCellValueError = Class.new(StandardError)

  def initialize(value = 0)
    raise InvalidCellValueError, value unless POSSIBLE_VALUES.include? value
    @value = value
  end

  attr_reader :value

  def empty?
    @value == 0
  end

  def ==(c) 
    c.value == value and not empty?
  end

  def next_value
    NEXT_VALUES[value]
  end

  def display
    cell_text = empty? ? '' : value.to_s
    cell_text.rjust(4)
  end

  private 

  POSSIBLE_VALUES = [0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]
  NEXT_VALUES = {}
  (1..10).each { |i| NEXT_VALUES[2**i] = 2**(i+1) }
end