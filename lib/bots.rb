$:.unshift File.dirname(__FILE__)
require 'board'

def play(bot_class)
  board = Board.new
  bot = bot_class.new(board)
  move_count = 0
  while board.game_still_on? and not board.full?
    board.add_new_cell
    bot.move!
    move_count += 1
  end
  print_board board
  puts "Move count: #{move_count}"
  board
end

def simulate_many_games
  100.times do
    board = play StupidBot
    puts "Hell Yeah!" if board.won?
  end
end
  
def print_board(board)
  board.to_a.each{|line| puts line.map{|c| c.to_s.rjust(5) }.join }
end

class StupidBot 
  def initialize(board)
    @board = board
  end

  def move!
    direction_priority = [:up, :left, :right, :down]
    first_good_direction = direction_priority.find{|d| @board.can_move? d}
    @board.move! first_good_direction
  end
end

class StupidBot2
  def initialize(board)
    @board = board
  end

  def move!
    direction_priority = (@last_move == :left) ? [:left, :up, :right, :down] : [:up, :left, :right, :down]
    direction = direction_priority.find{|d| @board.can_move? d}
    
    @board.move! direction
    @last_move = direction
  end
end


play StupidBot2
# simulate_many_games


