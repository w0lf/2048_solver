# encoding: UTF-8
require 'cell'
require 'transformable_2d_array'

class Board
  SIZE = 4
  GOAL = 2048

  def initialize
    @cells = Transformable2DArray.new(SIZE, Cell.new)
  end

  attr_accessor :cells

  def self.create_from_state(state)
    b = Board.new
    b.set_state(state)
    b
  end

  def empty?
    Board.iterate_coords{|row, col| 
      return false unless @cells[row, col].empty? 
    }
    true    
  end

  def full?
    Board.iterate_coords{|row, col| 
      return false if @cells[row, col].empty? 
    }
    true
  end

  def won?
    Board.iterate_coords{|row, col| 
      return true if @cells[row, col].value == GOAL
    }
    false
  end

  def lost?
    full? and stuck? and !won?
  end

  def game_over?
    won? or lost?
  end

  def game_still_on?
    not game_over?
  end

  def add_new_cell
    raise 'Cannot place new tile in full board!' if full?
    begin
      rand_x = rand(SIZE)
      rand_y = rand(SIZE)
    end until @cells[rand_x, rand_y].empty?
    new_cell_value = rand(10) == 0 ? 4 : 2
    @cells[rand_x, rand_y] = Cell.new new_cell_value
  end

  def stuck?
    [:left, :right, :up, :down].all?{|dir| not can_move? dir}
  end

  def can_move?(direction)
    transform_for direction

    lines = @cells.to_a.map{|l|
      l.map{|c| Cell.new(c.value)}.to_a
    }.to_a

    transform_for direction

    lines.any?{ |l| can_move_left(l) }
  end

  def can_move_left(line)
    original_values = line.map{ |c| c.value }.to_a
    squash_line_to_left(line)
    squashed_values = line.map{ |c| c.value }.to_a

    original_values != squashed_values
  end

  def move!(direction)
    transform_for direction
    (0...SIZE).each do |row|
      squash_line_to_left(@cells.line(row))
    end
    transform_for direction
  end

  def transform_for(direction)
    case direction
    when :right
      @cells.flip_horizontally!
    when :up
      @cells.transpose!
    when :down
      @cells.flip_horizontally!
      @cells.flip_vertically!
      @cells.transpose!      
    end
  end

  def squash_line_to_left(line)
    moved = false
    merged_until = -1

    (0...SIZE).each do |col|
      next if line[col].empty?

      shift_amount = 0
      merge = false

      (0...col).to_a.reverse_each do |col_to_left|
        if line[col_to_left].empty?
          shift_amount += 1
        else
          if col_to_left > merged_until and line[col_to_left] == line[col]
            merge = true
            merged_until = col_to_left
            shift_amount += 1
          end
          break
        end
      end
      if shift_amount > 0
        moved = true
        dest_index = col - shift_amount
        new_value = merge ? Cell.new(line[col].value * 2) : line[col]
        line[dest_index] = new_value
        line[col] = Cell.new
      end
    end
    moved
  end

  def to_a
    (0...SIZE).map do |i|
      @cells.line(i).map{|c| c.value}.to_a
    end.to_a
  end

  def set_state(state)
    Board.iterate_coords{|x, y|
      @cells[x, y] = Cell.new(state[x][y])
    }
  end

  def self.iterate_coords()
    (0...SIZE).map{|i| (0...SIZE).map{|j| yield [i, j] }}
  end

  private :squash_line_to_left, :transform_for

end