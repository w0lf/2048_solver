# encoding: UTF-8
require 'spec_helper'

describe Cell do
  context 'initialization' do
    it 'is initialized as empty by default' do
      subject.should be_empty
    end

    it 'can be initialized using value' do
      c = Cell.new(2048)
      c.value.should == 2048
      c.should_not be_empty

      Cell::InvalidCellValueError
    end

    it 'cannot be initialized using invalid (non 0 or powers of 2) values' do
      [1, 3, 5, 6, 7, 1000].each do |i|
        expect { Cell.new(i) }.to raise_error(Cell::InvalidCellValueError)
      end
    end
  end

  context 'equality' do
    specify { Cell.new(2).should == Cell.new(2) }
    specify { Cell.new(16).should == Cell.new(16) }

    specify 'empty Cells are not equal' do
      Cell.new().should_not == Cell.new()
    end
  end

  context 'next value' do
    it 'has no next value when empty' do
      Cell.new().next_value.should be_nil
    end

    it 'has no next value when max value (2048)' do
      Cell.new(2048).next_value.should be_nil
    end

    it 'returns next power of 2' do
      Cell.new(2).next_value.should == 4
      Cell.new(16).next_value.should == 32
      Cell.new(1024).next_value.should == 2048
    end
  end

  context 'display' do
    it 'prints value 2' do
      Cell.new(2).display.should == '   2'
    end

    it 'prints value 2048' do
      Cell.new(2048).display.should == '2048'
    end

    it 'prints empty box for value 0' do
      Cell.new(0).display.should == '    '
    end
  end
end