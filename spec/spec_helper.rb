require 'rspec'

require_relative '../lib/board'
require_relative '../lib/transformable_2d_array'

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end