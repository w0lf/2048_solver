# encoding: UTF-8
require 'spec_helper'

describe Board do
  context 'at the beginning of the game' do
    it 'creates empty board' do
      subject.should be_empty
    end

    it 'creates board from array' do
      b = Board.create_from_state([
        [32, 16, 0, 0],
        [16,  8, 0, 0],
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
        ])
      b.should_not be_empty
    end

    it 'export board to array' do
      b = Board.new
      b.to_a.should == [
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
      ]
    end

    it 'can add a random non-zero cell' do
      b = Board.new
      b.should be_empty

      b.add_new_cell

      b.should_not be_empty
    end
  end

  context 'cell movement:' do
    subject { Board.create_from_state([
      [32, 16, 0, 0],
      [16,  8, 0, 0],
      [ 0,  0, 2, 0],
      [ 0,  0, 0, 0],
      ])}

    it 'can move cells to the right' do
      subject.move! :right
      subject.to_a.should == [
        [ 0,  0, 32, 16],
        [ 0,  0, 16,  8],
        [ 0,  0,  0,  2],
        [ 0,  0,  0,  0],
      ]
    end

    it 'can move cells to the left' do
      subject.move! :left
      subject.to_a.should == [
        [32, 16, 0, 0],
        [16,  8, 0, 0],
        [ 2,  0, 0, 0],
        [ 0,  0, 0, 0],
      ]
    end

    it 'can move cells up' do
      subject.move! :up
      subject.to_a.should == [
        [32, 16, 2, 0],
        [16,  8, 0, 0],
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
      ]
    end

    it 'can move cells down' do
      subject.move! :down
      subject.to_a.should == [
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
        [32, 16, 0, 0],
        [16,  8, 2, 0],
      ]
    end

    context 'simple cell merging' do
      subject { Board.create_from_state([
        [16, 16, 4, 0],
        [16,  8, 4, 0],
        [ 2,  0, 2, 0],
        [ 0,  0, 2, 0],
        ])}

      it 'merges when moving left' do
        subject.move! :left
        subject.to_a.should == [
        [32,  4, 0, 0],
        [16,  8, 4, 0],
        [ 4,  0, 0, 0],
        [ 2,  0, 0, 0],
        ] 
      end

      it 'merges when moving right' do
        subject.move! :right
        subject.to_a.should == [
        [0,  0, 32, 4],
        [0, 16,  8, 4],
        [0,  0,  0, 4],
        [0,  0,  0, 2],
        ] 
      end

      it 'merges when moving up' do
        subject.move! :up
        subject.to_a.should == [
        [32, 16, 8, 0],
        [ 2,  8, 4, 0],
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
        ] 
      end

      it 'merges when moving down' do
        subject.move! :down
        subject.to_a.should == [
        [ 0,  0, 0, 0],
        [ 0,  0, 0, 0],
        [32, 16, 8, 0],
        [ 2,  8, 4, 0],
        ] 
      end
    end

    context 'does not merge twice in one move' do
      subject { Board.create_from_state([
        [ 8,  8, 16,  0],
        [ 0,  0,  0,  0],
        [ 0,  0,  0,  0],
        [ 0,  0,  0,  0],
        ])}

      it 'only merges each pair' do
        subject.move! :left
        subject.to_a.should == [
          [16, 16,  0,  0],
          [ 0,  0,  0,  0],
          [ 0,  0,  0,  0],
          [ 0,  0,  0,  0],
        ]
      end
    end

    describe '#can_move?' do
      let (:slide_board) { Board.create_from_state([
          [32, 16,  0,  0],
          [ 8,  0,  0,  0],
          [ 4,  0,  0,  0],
          [ 2,  0,  0,  0],
        ])}

      it 'cannot move if no cells would slide/merge' do
        expect(slide_board.can_move?(:left)).to be false
      end
      it 'can move if any cell would slide' do
        expect(slide_board.can_move?(:right)).to be true
      end

      it 'can move if any cells would merge' do
        can_move = Board.create_from_state([
          [32, 32,  0,  0],
          [ 8,  0,  0,  0],
          [ 4,  0,  0,  0],
          [ 2,  0,  0,  0],
        ]).can_move?(:left)

        expect(can_move).to be true
      end

      it 'recognizes a stuck board' do
        stuck = Board.create_from_state([
          [32,  2,  32,  2],
          [ 2, 32,  2,  32],
          [32,  2,  32,  2],
          [ 2, 32,  2,  32],
        ]).stuck?

        expect(stuck).to be true
      end
    end
  end
end