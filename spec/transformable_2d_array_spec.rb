require 'spec_helper'

describe Transformable2DArray do
  context 'when created' do
    it 'has the dimension passed to it' do
      a = Transformable2DArray.new(4)
      a.size.should == 4
    end

    it 'contains default elements' do
      a = Transformable2DArray.new(2, 'default')
      a[0, 0].should == 'default'
      a[0, 1].should == 'default'
      a[1, 0].should == 'default'
      a[1, 1].should == 'default'
    end
  end

  context 'accessing values' do
    let(:a) { Transformable2DArray.new(3, 'default') }

    it 'can write and read values' do
      a[2, 1] = 'non-default'

      a[2, 0].should == 'default'      
      a[2, 1].should == 'non-default'      
    end

    it 'rejects negative coordinates' do
      expect{ a[-1, -1] }.to raise_error('Coordinates must be >= 0!')
      expect{ a[-1, -1] = 3 }.to raise_error('Coordinates must be >= 0!')      
    end

    it 'rejects coordinates exceeding size' do
      expect{ a[3, 3] }.to raise_error('Coordinates cannot exceed size!')
      expect{ a[3, 3] = 3 }.to raise_error('Coordinates cannot exceed size!')      
    end

    it 'can have all values set by passing an array of arrays' do
      a.set_values([
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        ])

      a[1, 1].should == 4
    end

    it 'can expose values of a line' do
      a.line(1).should == ['default'] * 3
    end
  end

  context 'transformations: ' do

    let(:a) {
      a = Transformable2DArray.new(3)
      a.set_values([
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        ])
      a
    }

    it 'flips values horizontally' do
      a.flip_horizontally!
      a.to_a.should == [
        [2, 1, 0],
        [5, 4, 3],
        [8, 7, 6]
      ]
    end

    it 'flips values vertically' do
      a.flip_vertically!
      a.to_a.should == [
        [6, 7, 8],
        [3, 4, 5],
        [0, 1, 2],
      ]
    end
    
    it 'transposes values along main diagonal' do
      a.transpose!
      a.to_a.should == [
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
      ]
    end
  end
end